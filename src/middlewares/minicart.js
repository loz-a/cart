import { REQUEST_PRODUCTS } from '../constants'

export default ({ dispatch }) => (next) => (action) => {
  console.log('hello in minicart middleware');
  if (action.type !== REQUEST_PRODUCTS) return next(actin)

  const storage = window.sessionStorage
  const cart = storage.getItem('cart')

  const payload = {
    cartIds: cart ? cart : {}
  }

  return next({...action, payload})
}
