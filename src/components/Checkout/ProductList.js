import React, { PropTypes } from 'react'
import { ProductPropType } from '../../utils/PropTypes'
import ListItem from './ListItem'
import { currency } from '../../utils/numeral'

const t = window.translations

const ProductList = ({
  products,
  grandTotal,
  hasProducts,
  loading,
  quantityIncrement,
  quantityDecrement,
  removeProduct
}) => { 
  if (loading) return (
    <div className="alert cart-alert alert-info">
      {t['loading']}
    </div>
  )

  if (!hasProducts) return (
    <div className="cart-alert alert alert-info">
      {t['Please add product to cart']}
    </div>
  )

  return (
    <table className="table checkout-table">
      <thead>
        <tr>
          <th>{t['Goods']}</th>
          <th>{t['Amount (packages)']}</th>
          <th>{t['Price (UAH)']}</th>
          <th>{t['Amount (UAH)']}</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        {products.map((product) => (
          <ListItem
            key={product.id}
            product={product}
            onIncrement={() => quantityIncrement(product.id)}
            onDecrement={() => quantityDecrement(product.id)}
            onRemove={() => removeProduct(product.id)}
          />
        ))}
      </tbody>
      <tfoot>
        <tr>
          <td colSpan="4">
             {t['Total']}:<strong>{currency(grandTotal)}</strong> {t['uah.']}
          </td>
          <td></td>
        </tr>
      </tfoot>
    </table>
  )
}

ProductList.propTypes = {
  products: PropTypes.arrayOf(ProductPropType),
  grandTotal: PropTypes.number.isRequired,
  hasProducts: PropTypes.bool.isRequired,
  loading: PropTypes.bool.isRequired,
  quantityIncrement: PropTypes.func.isRequired,
  quantityDecrement: PropTypes.func.isRequired,
  removeProduct: PropTypes.func.isRequired
}

export default ProductList
