import React, { PropTypes } from 'react'
import { Link } from 'react-router'

const t = window.translations

const Navigation = ({ hasProducts }) => (
  <div className="bottom-navigation clearfix">
    {hasProducts &&
      <Link        
        to="/contacts-info"
        className="btn btn-link pull-right"
      >
        {t['Fill contact information']}
        <i className="icon next-step-icon"></i>
      </Link>
    }
  </div>
)

Navigation.propTypes = {
  hasProducts: PropTypes.bool.isRequired
}

export default Navigation
