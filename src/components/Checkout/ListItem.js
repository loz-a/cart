import React, { PropTypes } from 'react'
import { ProductPropType } from '../../utils/PropTypes'
import { currency } from '../../utils/numeral'
import Product from '../Product'

const t = window.translations

const ListItem = ({
  product,
  onIncrement,
  onDecrement,
  onRemove
}) => { 
  return(
  <tr>
    <td>
      <Product product={product}/>     
    </td>
    <td>
      <button onClick={onDecrement}> - </button>
      {' '}
      {product.quantity}
      {' '}
      <button onClick={onIncrement}> + </button>
    </td>
    <td>
      {currency(product.price)}
    </td>
    <td>
      {currency(product.totalPrice)}
    </td>
    <td className="remove-box">
      <a
        className="remove-icon"
        title={t['Delete item']}
        onClick={onRemove}>
      </a>
    </td>
  </tr>
)}

ListItem.propTypes = {
  product: ProductPropType.isRequired,
  onIncrement: PropTypes.func.isRequired,
  onDecrement: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired
}

export default ListItem
