import React, { PropTypes } from 'react'
import { Link } from 'react-router'

const t = window.translations

class Navigation extends React.Component {

  state = {
    transitionCanceled: false
  }

  componentWillReceiveProps() {
    this.setState({
      transitionCanceled: false
    })
  }

  onConfirmTransition = (evt) => {
    const { hasInvalidContacts, hasRequiredContacts } = this.props

    if (hasInvalidContacts || !hasRequiredContacts) {
      this.setState({
        transitionCanceled: true
      })
      evt.preventDefault()
    }
  }

  render() {
    const { transitionCanceled } = this.state

    return (
      <div className="bottom-navigation clearfix">

        { transitionCanceled &&
          <div className="alert alert-error">
            {t['You did not fill all required fields or filled them not true']}
          </div>
        }

        <Link
          to="/checkout"
          className="btn btn-link pull-left"
        >
          <i className="icon prev-step-icon"></i>
          {t['View cart']}
        </Link>

        <Link
          to="/confirm"
          className="btn btn-link pull-right"
          onClick={this.onConfirmTransition}
        >
          {t['Confirm order']}
          <i className="icon next-step-icon"></i>
        </Link>
      </div>
    )
  }
}

Navigation.propTypes = {
  hasInvalidContacts: PropTypes.bool.isRequired,
  hasRequiredContacts: PropTypes.bool.isRequired
}

export default Navigation
