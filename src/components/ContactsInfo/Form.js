import React, { PropTypes } from 'react'
import MaskedInput from 'react-text-mask'
import emailMask from 'text-mask-addons/dist/emailMask'
import cx from 'classnames'
import { ContactsPropTypes } from '../../utils/PropTypes'
import validate from '../../validators/ContactsInfo'

const t = window.translations

class Form extends React.Component {

  onBlur = (evt) => {
    const name = evt.target.name
    const value = evt.target.value

    if (!validate[name]
      || (validate[name] && validate[name](value) === true)
    ) {
        this.props['set' + name.charAt(0).toUpperCase() + name.slice(1)](value)
    }

    this.props.setInvalidContacts(validate.getErrors())
  }

  render() {
    const { contacts, invalidContacts } = this.props

    return (
      <form className="form-horizontal contacts-form">
        <div className={cx('control-group', {'error': invalidContacts['name']})}>
          <label className="control-label" htmlFor="inputName">{t['Your name']}*:</label>
          <div className="controls">
            <input
              name="name"
              defaultValue={contacts.name}
              onBlur={this.onBlur}
              id="inputName"
            />

            { invalidContacts['name']
              && <span className="help-inline">{invalidContacts['name']}</span>
            }

          </div>
        </div>

        <div className={cx('control-group', {'error': invalidContacts['phone']})}>
          <label className="control-label" htmlFor="inputPhone">{t['Mob.']}*:</label>
          <div className="controls">
            <MaskedInput
              mask={['+', '3', '8', ' ', '(', /[0-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/]}
              name="phone"
              defaultValue={contacts.phone}
              onBlur={this.onBlur}
              id="inputPhone"
            />

            { invalidContacts['phone']
              && <span className="help-inline">{invalidContacts['phone']}</span>
            }

          </div>
        </div>

        <div className={cx('control-group', {'error': invalidContacts['email']})}>
          <label className="control-label" htmlFor="inputEmail">{t['E-mail']}*:</label>
          <div className="controls">
            <MaskedInput
              mask={emailMask}
              name="email"
              defaultValue={contacts.email}
              onBlur={this.onBlur}
              placeholder="john@smith.com"
              id="inputEmail"
            />

            {invalidContacts['email']
              && <span className="help-inline">{invalidContacts['email']}</span>
            }

          </div>
        </div>

        <div className="control-group">
          <label className="control-label" htmlFor="inputNote">{t['Note']}:</label>
          <div className="controls">
            <textarea
              name="note"
              defaultValue={contacts.note}
              onBlur={this.onBlur}
              id="inputNote"
            />
          </div>
        </div>
        <div className="alert alert-block">
          <strong>{t['Warning']}!</strong> {t['Fields marked with an asterisk (*) are required']}
        </div>
      </form>
    )
  }
}

Form.propTypes = {
  contacts: ContactsPropTypes.isRequired,
  setName: PropTypes.func.isRequired,
  setPhone: PropTypes.func.isRequired,
  setEmail: PropTypes.func.isRequired,
  setNote: PropTypes.func.isRequired,
  invalidContacts: PropTypes.object.isRequired,
  setInvalidContacts: PropTypes.func.isRequired
}

export default Form