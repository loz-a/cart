import React, { PropTypes } from 'react'

const t = window.translations

const Confirmed = ({
  isLoading,
  isConfirm
}) => {
  if (isLoading) return <p>{t['loading']}...</p>

  return (
    <div>
        { !isConfirm &&
          <div className="alert cart-alert alert-error">
            <strong>{t['attention']}!!!</strong> {t['Something went wrong and we could not get your order.']}
              {t['We will be very grateful if you will inform us about this case.']}
            <a href="/index.html#contact">{t['Our contacts']}</a>
          </div>
        }

        { isConfirm &&
          <div className="alert cart-alert alert-success">
            <strong>{t['Success']}!</strong> {t['We have received your order. Our sales representative will soon contact you to connectivity.']}
          </div>
        }
    </div>
  )
}

Confirmed.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  isConfirm: PropTypes.bool.isRequired
}

export default Confirmed
