import React, { PropTypes } from 'react'
import { ProductPropType } from '../utils/PropTypes'
import { currency } from '../utils/numeral'

const t = window.translations

const Product = ({ 
  product,
  addToCartBtn = null
}) => (
  <div className="media">
    <div className="pull-left">
      <img
        className="media-object"
        src={product.thumb}
        alt={product.model}
      />
    </div>
    <div className="media-body">
      <h4 className="media-heading">
        {product.model} <span className="sub-heading">({product.catalog})</span>
      </h4>
      <p>
        {t['In the box']} {product.palett} {t['pcs.']} {t['price per piece']} - {currency(product.unitPrice)} {t['uah.']}
        {/*{t['In the box']} {product.palett} {t['pcs.']} {t['price per piece']} - {currency(product.price)} {t['uah.']}*/}
      </p>
      <p className="details">
        { product.height && !isNaN(product.height) && `${t['height']} - ${product.height} mm, ` }
        { (!isNaN(product.volume)) && `${t['volume']} - ${product.volume} cm3, ` }
        { !isNaN(product.weight) && `${t['weight']} - ${product.weight} g, ` }
        { !isNaN(product.diameter) && `${t['diameter']} - ${product.diameter} mm, ` }
        { product.manhole && `${t['manhole']} - ${product.manhole}, ` }
        { product.color && `${t['color']} ${product.color}, ` }
        { product.material && `${t['material product']} ${product.material}` }
      </p>
      { addToCartBtn }
    </div>
  </div>
)

Product.propTypes = {
  product: ProductPropType.isRequired,
  addToCartBtn: PropTypes.element
}

export default Product
