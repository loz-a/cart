import React from 'react'

const t = window.translations

const NotFoundPage = () => (
  <div className="alert cart-alert alert-error">
    {t['something was wrong']}
  </div>
)

export default NotFoundPage
