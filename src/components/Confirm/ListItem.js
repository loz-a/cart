import React, { PropTypes } from 'react'
import { ProductPropType } from '../../utils/PropTypes'
import { currency } from '../../utils/numeral'
import Product from '../Product'

const t = window.translations

const ListItem = ({ product }) => (
  <tr>
    <td>
      <Product product={product} />      
    </td>
    <td>
      {product.quantity}
    </td>
    <td>
      {currency(product.price)}
    </td>
    <td>
      {currency(product.totalPrice)}
    </td>
  </tr>
)

ListItem.propTypes = {
  product: ProductPropType.isRequired
}

export default ListItem
