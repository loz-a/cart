import React, { PropTypes } from 'react'
import { ContactsPropTypes } from '../../utils/PropTypes'

const t = window.translations

const ContactsList = ({ contacts }) => (
  <dl className="dl-horizontal">
    <dt>{t['Your name']}:</dt>
    <dd>{contacts.name}</dd>

    <dt>{t['Mob.']}:</dt>
    <dd>{contacts.phone}</dd>

    {contacts.email && <dt>{t['E-mail']}:</dt>}
    {contacts.email && <dd>{contacts.email}</dd>}

    {contacts.note && <dt>{t['Note']}:</dt>}
    {contacts.note && <dd>{contacts.note}</dd>}
  </dl>
)

ContactsList.propTypes = {
  contacts: ContactsPropTypes.isRequired,
}

export default ContactsList
