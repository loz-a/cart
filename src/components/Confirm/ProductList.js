import React, { PropTypes } from 'react'
import ListItem from './ListItem'
import { ProductPropType } from '../../utils/PropTypes'
import { currency } from '../../utils/numeral'

const t = window.translations

const ProductList = ({ products, grandTotal }) => (
  <table className="table checkout-table">
    <thead>
      <tr>
        <th>{t['Goods']}</th>
        <th>{t['Quantity']}</th>
        <th>{t['Price (UAH)']}</th>
        <th>{t['Amount (UAH)']}</th>
      </tr>
    </thead>
    <tbody>
      {products.map((product) => (
        <ListItem key={product.id} product={product} />
      ))}
    </tbody>
    <tfoot>
      <tr>
        <td colSpan="4">
          Всього: <strong>{currency(grandTotal)}</strong> {t['uah.']}
        </td>
      </tr>
    </tfoot>
  </table>
)


ProductList.propTypes = {
  products: PropTypes.arrayOf(ProductPropType).isRequired,
  grandTotal: PropTypes.number.isRequired
}

export default ProductList
