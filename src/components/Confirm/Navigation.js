import React, { PropTypes } from 'react'
import { Link } from 'react-router'

const t = window.translations

const Navigation = () => (
  <div className="bottom-navigation clearfix">
    <Link to="/contacts-info" className="btn btn-link pull-left">
      <i className="icon prev-step-icon"></i>
      {t['Fill contact information']}
    </Link>

    <Link to="/confirmed" className="btn btn-link pull-right">
    <i className="icon check-icon"></i>
      {t['Confirm order']}
    </Link>
  </div>
)

export default Navigation
