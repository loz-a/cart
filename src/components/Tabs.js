import React, { PropTypes } from 'react'

const t = window.translations

const Tabs = ({
  routes
}) => {
  const lastRoute = (routes.length) ? routes[routes.length - 1] : {}
  const currPath = lastRoute.path || ''

  const preventClick = (evt) => evt.preventDefault()

  return (
    <ul className="nav nav-tabs">
      <li className={currPath == 'checkout' ? 'active' : ''}>
        <a href="#" onClick={preventClick}>
          <i className="tab-icon cart-icon"></i>
          {t['basket']}
        </a>
      </li>
      <li className={currPath == 'contacts-info' ? 'active' : ''}>
        <a href="#" onClick={preventClick}>
          <i className="tab-icon contacts-icon"></i>
          {t['contacts']}
        </a>
      </li>
      <li className={currPath == 'confirm' ? 'active' : ''}>
        <a href="#" onClick={preventClick}>
          <i className="tab-icon confirm-icon"></i>
          {t['confirm']}
        </a>
      </li>
    </ul>
  )
}

Tabs.propTypes = {
  routes: PropTypes.arrayOf(PropTypes.object).isRequired
}

export default Tabs
