import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import Product from '../Product'
import { ProductPropType } from '../../utils/PropTypes'
import { addProduct } from '../../actions/checkout'

class RelatedProducts extends React.Component {

  render() {
    const { products, loaded } = this.props

    if (!Object.keys(products).length) return null

    return (
      <div className="related-products">
        <h2>З цим товаром найчастіше беруть!!!:</h2>
        <ul className="inline">
          {this.renderItems()}
        </ul>
      </div>      
    )
  }

  renderItems = () => {
    const { products, loaded } = this.props      
    if (!loaded) return null

    return products
    // Object.entries(products)
      .sort((ent1, ent2) => {       
        return ent1[0] < ent2[0] ? 1 : -1
      })
      .map((product) => {        
        const addToCartBtnElem = this.renderAddToCartBtn(product)
        return (
          <li key={product.id}>
            <Product product={product} addToCartBtn={addToCartBtnElem}/>            
          </li>
      )
    })
  }

  renderAddToCartBtn = (product) => {
    return (
      <div className="add-to-cart-box">
        <a href="#"
          className="btn-inverse cart-icon add-to-cart-btn" 
          onClick={this.handleAddToCart(product)}>
          В корзину
        </a>
      </div>
    )
  }

  handleAddToCart = (product) => (evt) => {
    evt.preventDefault()
    this.props.addProduct(product)
  }
}

RelatedProducts.propTypes = {
  products: PropTypes.arrayOf(ProductPropType).isRequired,
  loaded: PropTypes.bool.isRequired,
  addProduct: PropTypes.func.isRequired
}

export default connect(
  null,
  {
    addProduct
  }
)(RelatedProducts)