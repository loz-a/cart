import numeral from 'numeral'
import language from 'numeral/languages/uk-UA'

numeral.language('uk-UA', language)
numeral.language('uk-UA')

export default numeral

export const currency = (value) => numeral(value).format('0,0.00', Math.ceil)
