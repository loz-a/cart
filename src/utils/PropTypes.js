import { PropTypes } from 'react'

export const ProductPropType = PropTypes.shape({
    id: PropTypes.number.isRequired,
    catalogId: PropTypes.number.isRequired,
    catalog: PropTypes.string.isRequired,
    diameter: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    thumb: PropTypes.string.isRequired,
    manhole: PropTypes.string,
    model: PropTypes.string.isRequired,
    palett: PropTypes.number.isRequired,
    volume: PropTypes.number.isRequired,
    weight: PropTypes.number.isRequired,
    price: PropTypes.number.isRequired,
    totalPrice: PropTypes.number,
    quantity: PropTypes.number
})

export const ContactsPropTypes = PropTypes.shape({
    name: PropTypes.string.isRequired,
    phone: PropTypes.string.isRequired,
    email: PropTypes.string,
    note: PropTypes.string,
})
