import isEmail from 'validator/lib/isEmail'
import { isMobilePhone } from './validators'

const t = window.translations

const Validator = function() {
  let errors = {}

  return {

    name(value) {
      if (!value) {
        return errors['name'] = t['This field is required']
      }
      errors['name'] && delete errors['name']
      return true
    },

    phone(value) {
      if (!value) {
        return errors['phone'] = t['This field is required']
      }

      if (!isMobilePhone(value)) {
        return errors['phone'] = t['Entered mobile number is incorrect']
      }

      errors['phone'] && delete errors['phone']
      return true
    },

    email(value) {
      if (!value) {
        return errors['email'] = t['This field is required']
      }

      if (!isEmail(value)) {
        return errors['email'] = t['Entered Email is incorrect']
      }
      errors['email'] && delete errors['email']
      return true
    },

    validate(data) {
      errors = {}
      for (const key in data) if (data.hasOwnProperty(key)) {
        if (this[key]) this[key](data[key])
      }
      return !Object.keys(errors).length
    },

    getErrors() {
      return Object.assign({}, errors)
    }

  }
}

export default new Validator()
