export const isMobilePhone = (str) => {
  return /\+38\s?\(0\d{2}\)\s?\d{3}\-\d{2}\-\d{2}$/.test(str)
}
