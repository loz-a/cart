import React from 'react'
import { render } from 'react-dom'
import routes from './routes'

$(document).ready(() => {

  render(
    routes,
    document.getElementById('cart')
  )

})
