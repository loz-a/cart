import { combineReducers } from 'redux'

import {
  REQUEST_RELATED_PRODUCTS,
  RECEIVE_RELATED_PRODUCTS,
  RECEIVE_RELATED_PRODUCTS_FAILURE
} from '../constants'

const init = {
  byId: {},
  loading: false,
  loaded: false
}

function byId(state = init.byId, action) {
  switch (action.type) {
    case RECEIVE_RELATED_PRODUCTS: 
      const { relatedProducts } = action
      if (!relatedProducts) return state

      return {
        ...state,
        ...relatedProducts.reduce((acc, product) => {
          const id = '' + product.priority +  product.id
          acc[id] = {
            id: parseInt(product.id),
            catalogId: parseInt(product['catalog_id']),
            catalog: product['catalog_title'],
            color: product['color'],
            material: product['material'],
            diameter: parseInt(product['diameter']),
            height: parseInt(product['height']),
            thumb: product['img_src'],
            manhole: product['manhole'],
            model: product['name'],
            palett: parseInt(product['palett']),
            price: parseFloat(product['palett_height']),
            priceUah: parseInt(product['price_uah']) / 100,
            volume: parseInt(product['volume']),
            weight: parseInt(product['weight'])            
          }
          return acc
        }, {})
      }

    default: 
      return state
  }
}

function loading(state = init.loading, action) {
  switch (action.type) {
    case REQUEST_RELATED_PRODUCTS:
      return true
    case RECEIVE_RELATED_PRODUCTS:
    case RECEIVE_RELATED_PRODUCTS_FAILURE:
      return false
    default:
      return state
  }
}

function loaded(state = init.loaded, action) {
  switch (action.type) {    
    case RECEIVE_RELATED_PRODUCTS:    
      return true
    default:
      return state
  }
}

export default combineReducers({
  byId, 
  loaded,
  loading
})

export const getRelatedProducts = (state) => state.relatedProducts.byId

export const isLoaded = (state) => state.relatedProducts.loaded

export const isLoading = (state) => state.relatedProducts.loading