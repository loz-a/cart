import { combineReducers } from 'redux'
import { createSelector } from 'reselect'
import products, * as fromProducts from './product'
import relatedProducts, * as fromRelatedProducts from './relatedProducts'
import contacts, * as fromContacts from './contacts'
import confirm, * as fromConfirm from './confirm'
import currencyRate from './currency'

export default combineReducers({
  products,
  contacts,
  confirm,
  currencyRate,
  relatedProducts
})

// const getProduct = (state, id) => fromProducts.getProduct(state, id)

// const getProductsIds = (state) => fromProducts.getProductsIds(state)

export const getCurrencyRate = (state) => state.currencyRate

export const hasProducts = (state) => fromProducts.hasProducts(state)

export const getProductsLoading = (state) => fromProducts.getLoading(state)


export const getRelatedProducts = createSelector(
  fromRelatedProducts.getRelatedProducts,
  getCurrencyRate,
  (products, currencyRate) => {
    return Object.values(products)
      .reduce((acc, product) => {              
        const unitPrice  = product['priceUah'] ? product['priceUah'] :  product['price'] * currencyRate
        const price      = product['palett'] * unitPrice
        
        acc.push({
          ...product,          
          price,
          unitPrice
        })

        return acc
      }, [])
  }
)

export const isRelatedProductsLoaded = (state) => fromRelatedProducts.isLoaded(state)

export const isRelatedProductsLoading = (state) => fromRelatedProducts.isLoading(state)



export const getContacts = (state) => fromContacts.getContacts(state)

export const hasRequiredContacts = (state) => fromContacts.hasRequiredContacts(state)

export const getInvalidContacts = (state) => fromContacts.getInvalidContacts(state)

export const hasInvalidContacts = (state) => fromContacts.hasInvalidContacts(state)

export const isLoading = (state) => fromConfirm.isLoading(state)

export const isConfirm = (state) => fromConfirm.isConfirm(state)

export const getCartProductsAsArray = createSelector(
  fromProducts.getProducts,
  fromProducts.getProductsQuantity,
  getCurrencyRate,
  (products, productsQuantity, currencyRate) =>
    Object.keys(products)
      .reduce((acc, id) => {
        const product    = products[id]
        const quantity   = productsQuantity[id]
        const unitPrice  = product['priceUah'] ? product['priceUah'] :  product['price'] * currencyRate
        const price      = product['palett'] * unitPrice
        const totalPrice = price * quantity

        acc.push({
          ...product,
          quantity,
          price,
          unitPrice,
          totalPrice
        })

        return acc
      }, [])
)

export const getGrandTotal = createSelector(
  getCartProductsAsArray,
  (cartProducts) =>
    cartProducts.reduce((acc, product) => acc + product.totalPrice, 0)
)
