import { combineReducers } from 'redux'
import {
  ORDER_REQUEST,
  ORDER_CONFIRMED,
  ORDER_SEND_FAILURE
} from '../constants'

function result(state = false, action) {
  switch (action.type) {
    case ORDER_CONFIRMED:
      return true
    case ORDER_SEND_FAILURE:
      return false
    default:
      return state
  }
}

function loading(state = false, action) {
  switch (action.type) {
    case ORDER_REQUEST:
      return true
    case ORDER_CONFIRMED:
    case ORDER_SEND_FAILURE:
      return false
    default:
      return state
  }
}

export default combineReducers({
  loading,
  result
})

export const isLoading = (state) => state.confirm.loading

export const isConfirm = (state) => state.confirm.result
