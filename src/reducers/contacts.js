import { combineReducers } from 'redux'
import { CONTACTS_ADD, INVALID_CONTACTS_SET, CART_RESET } from '../constants'

const init = {
  byName: {
    name: '',
    phone: '',
    email: '',
    note: ''
  },
  invalidContacts: {}
}

export function byName(state = init.byName, action) {
  const { payload } = action
  switch (action.type) {
    case CONTACTS_ADD:
      return { ...state,...{[payload.name]: payload.value} }
    case CART_RESET:
      return init.byName
    default:
      return state
  }
}

export function invalidContacts(state = init.invalidContacts, action) {
  const { payload } = action
  switch (action.type) {
    case INVALID_CONTACTS_SET:
      return { ...payload.invalidContacts }
    case CART_RESET:
      return init.invalidContacts
    default:
      return state
  }
}

export default combineReducers({
  byName,
  invalidContacts
})

export const getContacts = (state) => state.contacts.byName

export const hasRequiredContacts = (state) => {
  const { name, phone, email } = state.contacts.byName
  return name.length > 0 && phone.length > 0 && email.length > 0
}

export const getInvalidContacts = (state) => state.contacts.invalidContacts

export const hasInvalidContacts = (state) => Object.keys(state.contacts.invalidContacts).length > 0
