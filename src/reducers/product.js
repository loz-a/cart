import { combineReducers } from 'redux'
import {
  REQUEST_PRODUCTS,
  RECEIVE_PRODUCTS,
  RECEIVE_PRODUCTS_FAILURE,
  PRODUCT_QUANTITY_INC,
  PRODUCT_QUANTITY_DEC,
  PRODUCT_ADD,
  PRODUCT_REMOVE,
  CART_RESET
} from '../constants'


const init = {
  byId: {},
  loading: false,
  quantityById: {}
}

function byId(state = init.byId, action) {
  switch (action.type) {
    case RECEIVE_PRODUCTS:
      const { products } = action
      if (!products) return state

      return {
        ...state,
        ...products.reduce((obj, product) => {
          obj[product.id] = {
            id: parseInt(product['id']),
            catalogId: parseInt(product['catalog_id']),
            catalog: product['catalog_title'],
            color: product['color'],
            material: product['material'],
            diameter: parseInt(product['diameter']),
            height: parseInt(product['height']),
            thumb: product['img_src'],
            manhole: product['manhole'],
            model: product['name'],
            palett: parseInt(product['palett']),
            price: parseFloat(product['palett_height']),
            priceUah: parseInt(product['price_uah']) / 100,
            volume: parseInt(product['volume']),
            weight: parseInt(product['weight'])
          }
          return obj
        }, {})
      }

    case PRODUCT_REMOVE:
      const {productId} = action
      return Object
        .keys(state)
        .reduce((obj, id) => {
          if(id != productId) obj[id] = state[id]
          return obj
        }, {})
      
    case PRODUCT_ADD:
      const { product } = action
      return {
        ...state,
        [product.id]: product
      }

    case CART_RESET:
        return init.byId

    default:
      return state
  }
}

function loading(state = init.loading, action) {
  switch (action.type) {
    case REQUEST_PRODUCTS:    
      return true
    case RECEIVE_PRODUCTS:
    case RECEIVE_PRODUCTS_FAILURE:    
      return false
    case CART_RESET:
      return init.loading
    default:
      return state
  }
}

function quantityById(state = init.quantityById, action) {
  switch (action.type) {
    case RECEIVE_PRODUCTS:
      const { products } = action
      if (!products) return state

      return {
        ...state,
        ...products.reduce((obj, product) => {
            obj[product.id] = 1
            return obj
          }, {})
        }

    case PRODUCT_QUANTITY_INC:
      const incId = action.productId
      return {
        ...state,
        ...{[incId]: state[incId] + 1}
      }

    case PRODUCT_QUANTITY_DEC:
      const decId = action.productId
      if (state[decId] == 1) return state
      return {
        ...state,
        ...{[decId]: state[decId] - 1}
      }

    case PRODUCT_REMOVE:
      const { productId } = action
      return Object
        .keys(state)
        .reduce((obj, id) => {
          if(id != productId) obj[id] = state[id]
          return obj
        }, {})
      
    case PRODUCT_ADD:
      const { product } = action
      return {
        ...state,
        [product.id]: 1
      }

    case CART_RESET:
      return init.quantityById

    default:
      return state
  }
}

export default combineReducers({
  byId,
  loading,
  quantityById
})

export const getProduct = (state, id) => state.products.byId[id]

export const getProducts = (state) => state.products.byId

export const getProductsQuantity = (state) => state.products.quantityById

// export const getProductsIds = (state) => Object.keys(state.products.byId)

export const hasProducts = (state) => Object.keys(state.products.byId).length > 0

export const getLoading = (state) => state.products.loading

export const getQuantityById = (state, id) => state.products.quantityById[id]
