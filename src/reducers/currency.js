import { CURRENCY_RATE_SET } from '../constants'

function currencyRate(state = 0, action) {
  switch (action.type) {
    case CURRENCY_RATE_SET:
      const { currencyRate } = action
      if (currencyRate) return currencyRate
    default:
      return state
  }
}

export default currencyRate
