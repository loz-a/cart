

function getCartItemsIds() {  
  return Object.keys(window.miniCart.getCartData())
}

export default {
  getCartProducts() {
    return $.get('cart.html', {ids: getCartItemsIds()})
  },

  getRelatedProducts() {
    return $.get('catalog/related_products', {ids: getCartItemsIds()})
  },

  sendOrder(products, contacts, grandTotal) {
    const productsData = {}
    for (const key in products) if (products.hasOwnProperty(key)) {
      productsData[products[key].id] = {
        quantity: products[key].quantity,
        price: products[key].price,
        totalPrice: products[key].totalPrice
      }
    }

    return $.post('cart.html', {
      products: productsData,
      contacts,
      grandTotal
    })
  },

  miniCart: {
    getIds: getCartItemsIds,
    setId(id) {
      window.miniCart.setId(id)
    },
    remove(id) {
      window.miniCart.remove(id)
    },
    clear() {
      window.miniCart.clear()
    }
  },

  getCurrencyRate() {
    const cartNode = document.getElementById('cart')

    let rate;
    if (cartNode.hasAttribute('data-valut-koef')) {
      rate = parseInt(cartNode.getAttribute('data-valut-koef'))
    }
    return rate || 0
  }
}
