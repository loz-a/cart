import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { fetchRelatedProducts } from '../actions/RelatedProducts'
import { 
  getRelatedProducts,
  isRelatedProductsLoaded,
  isRelatedProductsLoading
} from '../reducers'

import RelatedProducts from '../components/RelatedPrducts'
import { ProductPropType } from '../utils/PropTypes'

class RelatedProductsContainer extends React.Component {

  componentDidMount() {
    const { fetchRelatedProducts, loaded, loading } = this.props
    if (!loaded && !loading) fetchRelatedProducts()
  }

  render() {
    const { products, loaded } = this.props
    return (
      <RelatedProducts products={products} loaded={loaded} />
    )
  }  
}

RelatedProductsContainer.propTypes = {
  products: PropTypes.arrayOf(ProductPropType).isRequired,
  loaded: PropTypes.bool.isRequired,
  loading: PropTypes.bool.isRequired,
  fetchRelatedProducts: PropTypes.func.isRequired
}

export default connect(
  (state) => ({
    products: getRelatedProducts(state),
    loading: isRelatedProductsLoading(state),
    loaded: isRelatedProductsLoaded(state)
  }),
  {
    fetchRelatedProducts
  }
)(RelatedProductsContainer)

