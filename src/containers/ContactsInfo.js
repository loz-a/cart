import React from 'react'
import { connect } from 'react-redux'
import history from '../history'
import Form from '../components/ContactsInfo/Form'
import Navigation from '../components/ContactsInfo/Navigation'
import { setName, setPhone, setEmail, setNote, setInvalidContacts } from '../actions/contacts'
import {
  getContacts, hasRequiredContacts, getInvalidContacts,
  hasInvalidContacts, hasProducts
} from '../reducers'


class ContactsInfoContainer extends React.Component {

  componentWillMount() {
    const { hasProducts } = this.props
    if (!hasProducts) history.replace('/checkout')
  }

  render() {
    const {
      contacts,
      hasRequiredContacts,
      invalidContacts,
      hasInvalidContacts,
      setInvalidContacts,
      setName,
      setPhone,
      setEmail,
      setNote
    } = this.props

    return (
      <div>
        <Form
          contacts={contacts}
          invalidContacts={invalidContacts}
          setInvalidContacts={setInvalidContacts}
          setName={setName}
          setPhone={setPhone}
          setEmail={setEmail}
          setNote={setNote} />

        <Navigation
          hasInvalidContacts={hasInvalidContacts}
          hasRequiredContacts={hasRequiredContacts}/>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    contacts: getContacts(state),
    hasRequiredContacts: hasRequiredContacts(state),
    invalidContacts: getInvalidContacts(state),
    hasInvalidContacts: hasInvalidContacts(state),
    hasProducts: hasProducts(state)
  }),
  {
    setName,
    setPhone,
    setEmail,
    setNote,
    setInvalidContacts
  }
)(ContactsInfoContainer)
