import React from 'react'
import { Provider } from 'react-redux'
import store from '../store'
import Tabs from '../components/Tabs'
import RelatedProductsContainer from './RelatedProducts'

const AppContainer = ({
  routes,
  children
}) => {    
  return (
  <Provider store={store}>
    <div>
      <Tabs routes={routes} />
      {children}
      <RelatedProductsContainer/>  
    </div>
  </Provider>
)}

export default AppContainer
