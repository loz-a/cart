import React from 'react'
import { connect } from 'react-redux'
import history from '../history'

import {
  getCartProductsAsArray,
  getGrandTotal,
  getContacts,
  hasRequiredContacts,
  hasProducts
} from '../reducers'

import ProductList from '../components/Confirm/ProductList'
import ContactsList from '../components/Confirm/ContactsList'
import Navigation from '../components/Confirm/Navigation'

class ConfirmContainer extends React.Component {
  componentWillMount() {
    const { hasProducts, hasRequiredContacts } = this.props
    if (!hasProducts) history.replace('/checkout')
    if (!hasRequiredContacts) history.replace('/contacts-info')
  }

  render() {
    const { products, grandTotal, contacts } = this.props

    return (
      <div>
        <ProductList products={products} grandTotal={grandTotal} />
        <ContactsList contacts={contacts} />
        <Navigation />
      </div>
    )
  }
}

export default connect(
  (state) => ({
    products: getCartProductsAsArray(state),
    grandTotal: getGrandTotal(state),
    contacts: getContacts(state),
    hasRequiredContacts: hasRequiredContacts(state),
    hasProducts: hasProducts(state)
  })
)(ConfirmContainer)
