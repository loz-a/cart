import React from 'react'
import { connect } from 'react-redux'

import {
  fetchProducts,
  quantityIncrement,
  quantityDecrement,
  removeProduct
} from '../actions/checkout'

import { initCurrency } from '../actions/currency'

import {
  getCartProductsAsArray,
  getProductsLoading,
  hasProducts,
  getGrandTotal
} from '../reducers'

import ProductList from '../components/Checkout/ProductList'
import Navigation from '../components/Checkout/Navigation'

class CheckoutContainer extends React.Component {
  componentDidMount() {
    const { loading, products, fetchProducts, initCurrency } = this.props
    if (!loading && !products.length) fetchProducts()
    initCurrency()
  }

  render() {
    const {
      products,
      grandTotal,
      hasProducts,
      loading,
      quantityIncrement,
      quantityDecrement,
      removeProduct
    } = this.props

    return (
      <div>
        <ProductList
          products={products}
          grandTotal={grandTotal}
          hasProducts={hasProducts}
          loading={loading}
          quantityIncrement={quantityIncrement}
          quantityDecrement={quantityDecrement}
          removeProduct={removeProduct}
        />
        <Navigation hasProducts={hasProducts} />
      </div>
    )
  }
}

export default connect(
  (state) => ({
    products: getCartProductsAsArray(state),
    grandTotal: getGrandTotal(state),
    loading: getProductsLoading(state),
    hasProducts: hasProducts(state)
  }),
  {
    fetchProducts,
    quantityIncrement,
    quantityDecrement,
    removeProduct,
    initCurrency
  }
)(CheckoutContainer)
