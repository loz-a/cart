import React from 'react'
import { connect } from 'react-redux'
import history from '../history'

import {
  getCartProductsAsArray,
  getGrandTotal,
  getContacts,
  hasRequiredContacts,
  hasProducts,
  isLoading,
  isConfirm
} from '../reducers'

import { sendOrder } from '../actions/confirm'
import Confirmed from '../components/Confirmed'

class ConfirmContainer extends React.Component {
  componentDidMount() {
    const { hasProducts, hasRequiredContacts } = this.props

    if (!hasProducts) history.replace('/checkout')
    if (!hasRequiredContacts) history.replace('/contacts-info')

    const { products, grandTotal, contacts, sendOrder } = this.props
    sendOrder(products, contacts, grandTotal)
  }

  render() {
    const { isLoading, isConfirm } = this.props

    return (
      <Confirmed isLoading={isLoading} isConfirm={isConfirm} />
    )
  }
}

export default connect(
  (state) => ({
    products: getCartProductsAsArray(state),
    grandTotal: getGrandTotal(state),
    contacts: getContacts(state),
    hasRequiredContacts: hasRequiredContacts(state),
    hasProducts: hasProducts(state),
    isLoading: isLoading(state),
    isConfirm: isConfirm(state)
  }),
  {
    sendOrder
  }
)(ConfirmContainer)
