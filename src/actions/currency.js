import { CURRENCY_RATE_SET } from '../constants'
import api from '../api'

const setCurrencyRate = (currencyRate) => ({
  type: CURRENCY_RATE_SET,
  currencyRate
})

export const initCurrency = () =>
  (dispatch) => {
    dispatch(
      setCurrencyRate(api.getCurrencyRate())
    )
  }
