import {
  REQUEST_PRODUCTS,
  RECEIVE_PRODUCTS,
  RECEIVE_PRODUCTS_FAILURE,
  PRODUCT_QUANTITY_INC,
  PRODUCT_QUANTITY_DEC,
  PRODUCT_ADD,
  PRODUCT_REMOVE
} from '../constants'

import api from '../api'

const requestCartProducts = () => ({
  type: REQUEST_PRODUCTS
})

const receiveCartProducts = (products) => ({
  type: RECEIVE_PRODUCTS,
  products
})

const receiveCartProductsFailure = (error) => ({
  type: RECEIVE_PRODUCTS_FAILURE,
  error
})


export const fetchProducts = () =>
  (dispatch) => {
    dispatch(requestCartProducts())

    api.getCartProducts().then(
      (products) => dispatch(receiveCartProducts(products)),
      (error)    => dispatch(receiveCartProductsFailure(error))
    )
  }

export const quantityIncrement = (productId) => ({
  type: PRODUCT_QUANTITY_INC,
  productId
})

export const quantityDecrement = (productId) => ({
  type: PRODUCT_QUANTITY_DEC,
  productId
})

export const addProduct = (product) => 
  (dispatch) => {
    dispatch({
      type: PRODUCT_ADD,
      product
    })
    api.miniCart.setId(product.id)
  }

export const removeProduct = (productId) =>
  (dispatch) => {
    dispatch({
      type: PRODUCT_REMOVE,
      productId
    })

    api.miniCart.remove(productId)
  }