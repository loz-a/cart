import { CONTACTS_ADD, INVALID_CONTACTS_SET } from '../constants'

export const setName = (name) => ({
  type: CONTACTS_ADD,
  payload: {
    name: 'name',
    value: name
  }
})

export const setPhone = (phone) => ({
  type: CONTACTS_ADD,
  payload: {
    name: 'phone',
    value: phone
  }
})

export const setEmail = (email) => ({
  type: CONTACTS_ADD,
  payload: {
    name: 'email',
    value: email
  }
})

export const setNote = (note) => ({
  type: CONTACTS_ADD,
  payload: {
    name: 'note',
    value: note
  }
})

export const setInvalidContacts = (invalidContacts) => ({
  type: INVALID_CONTACTS_SET,
  payload: {
    invalidContacts
  }
})
