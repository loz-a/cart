import {
  ORDER_REQUEST,
  ORDER_CONFIRMED,
  ORDER_SEND_FAILURE,
  CART_RESET
} from '../constants'

import api from '../api'

const requestOrder = () => ({
  type: ORDER_REQUEST
})

const orderConfirmed = (confirmation) => ({
  type: ORDER_CONFIRMED,
  payload: {
    confirmation
  }
})

const orderSendFailuer = (error) => ({
  type: ORDER_SEND_FAILURE,
  payload: {
    error
  }
})

const resetCart = () =>
  (dispatch) => {
      dispatch({type: CART_RESET})
      api.miniCart.clear()
  }


export const sendOrder = (products, contacts, grandTotal) =>
  (dispatch) => {
    dispatch(requestOrder())

    api.sendOrder(products, contacts, grandTotal).then(
      (confirmation) => {
        dispatch(orderConfirmed(confirmation))
        dispatch(resetCart())
      },
      (error) => dispatch(orderSendFailuer(error))
    )
  }
