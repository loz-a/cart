import {
  REQUEST_RELATED_PRODUCTS,
  RECEIVE_RELATED_PRODUCTS,
  RECEIVE_RELATED_PRODUCTS_FAILURE
} from '../constants'

import api from '../api'

const requestRelatedProducts = () => ({
  type: REQUEST_RELATED_PRODUCTS
})

const receiveRelatedProducts = (relatedProducts) => ({
  type: RECEIVE_RELATED_PRODUCTS,
  relatedProducts
})

const receiveRelatedProductsFailure = (error) => ({
  type: RECEIVE_RELATED_PRODUCTS_FAILURE,
  error
})

export const fetchRelatedProducts = () => 
  (dispatch) => {
    dispatch(requestRelatedProducts())

    api.getRelatedProducts().then(
      (relatedProducts) => {        
        dispatch(receiveRelatedProducts(relatedProducts))
      },
      (error) => dispatch(receiveRelatedProductsFailure(error))
    )
  }

