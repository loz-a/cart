import { createStore, applyMiddleware, compose } from 'redux'
import reducers from '../reducers'
import thunk from 'redux-thunk'
import createLogger from 'redux-logger'

const middlewares = [thunk]

if (NODE_ENV === 'development') {
  middlewares.push(createLogger())
}

const enhancer = compose(
  applyMiddleware(...middlewares),
  window.devToolsExtension ? window.devToolsExtension() : (f) => f
)

const store = createStore(reducers, {}, enhancer)

if (NODE_ENV === 'development') {
  window.store = store
}

export default store
