import React from 'react'
import { Router, Route, Redirect } from 'react-router'
import history from './history'

import AppContainer from './containers/AppContainer'
import Checkout from './containers/Checkout'
import ContactsInfo from './containers/ContactsInfo'
import Confirm from './containers/Confirm'
import Confirmed from './containers/Confirmed'
import NotFoundPage from './components/NotFoundPage'

export default (
  <Router history={history}>
    <Redirect from="/" to="/checkout" />
    <Route path="/" component={AppContainer}>
      <Route path="checkout" component={Checkout} />
      <Route path="contacts-info" components={ContactsInfo} />
      <Route path="confirm" components={Confirm} />
      <Route path="confirmed" components={Confirmed} />
    </Route>
    <Route path="*" component={NotFoundPage} />
  </Router>
)
