const path = require('path');

const ENV_DEVELOPMENT = 'development'
const ENV_PRODUCTION  = 'production'

const NODE_ENV = process.env.NODE_ENV || ENV_DEVELOPMENT
const webpack = require('webpack')

module.exports = {
    devtool: NODE_ENV ? 'cheap-inline-module-source-map' : null,

    entry: {
        index: './src/index.js'
    },

    output: {
        path: path.join(__dirname, '..'),
        filename: 'cart.js'
    },

    watch: NODE_ENV == ENV_DEVELOPMENT,

    watchOptions: {
      aggregateTimeout: 100
    },

    module: {
        loaders: [
            {
                test: /\.js$/,
                loaders: ['babel'],
                exclude: /(node_modules|bower_components)/,
                include: path.join(__dirname, 'src')
            }, {
                test: /\.css/,
                loader: 'style-loader!css-loader'
            }
        ]
    },

    plugins: [
        new webpack.DefinePlugin({
            NODE_ENV: JSON.stringify(NODE_ENV)
        }),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin(),
        new webpack.optimize.DedupePlugin()
    ],

    resolve: {
        modulesDirectories: ['node_modules'],
        extensions: ['', '.js']
    },

    resolveLoader: {
        modulesDirectories: ['node_modules'],
        moduleTemplates: ['*-loader', '*'],
        extensions: ['', '.js']
    },
};

if (NODE_ENV === ENV_PRODUCTION) {
    module.exports.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false,
                drop_console: true,
                unsafe: true
            }
        })
    )
}
